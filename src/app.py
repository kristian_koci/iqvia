#src/app.py
from rq import Queue
from rq.job import Job
from .worker import conn

from flask import Flask
import requests

from .config import app_config
from .models import db, bcrypt

# import user_api blueprint
from .views.UserView import user_api as user_blueprint
from .views.BlogpostView import post_api as post_blueprint


def create_app(env_name):
  """
  Create app
  """
  
  # app initiliazation
  app = Flask(__name__)

  app.config.from_object(app_config[env_name])

  # initializing bcrypt and db
  bcrypt.init_app(app)
  db.init_app(app)
  
  app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
  app.register_blueprint(user_blueprint, url_prefix='/api/v1/users')
  app.register_blueprint(post_blueprint, url_prefix='/api/v1/posts')

  q = Queue(connection=conn)

  @app.route('/', methods=['GET', 'POST'])
  def index():
    """
    example endpoint
    """
    job = q.enqueue_call(
    func=create_app, args=(), result_ttl=5000
      )
    print(job.get_id())
    return 'Congratulations! Your endpoint is working'

  return app





