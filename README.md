# IQVIA TEST
This is a Blog Rest-API application based on Flask

## Installation
  - Install [Pipenv](https://docs.pipenv.org/) and [Postgres](https://www.postgresql.org/) on your machine
  - Clone the repository `$ git clone https://kristian_koci@bitbucket.org/kristian_koci/iqvia.git`
  - Change into the directory `$ cd /pharma_test`
  - Activate the project virtual environment with `$ pipenv shell` command
  - Install all required dependencies with `$ pipenv install`
  - Export the required environment variables
      ```
      $ export FLASK_ENV=development
      $ export DATABASE_URL=postgres://name:password@houst:port/blog_api_db
      $ export JWT_SECRET_KEY=hhgaghhgsdhdhdd
      ```
  - Start the app with `python run.py`
  - I'd recommend POSTMAN to make queries, insert users, posts, etc. https://www.getpostman.com/
  - The application uses JSON Web token for authentication.

## Tests

  - Export the required environment variables
      ```
      $ export FLASK_ENV=testing
      $ export DATABASE_TEST_URL=postgres://name:password@houst:port/blog_api_db
      $ export JWT_SECRET_KEY=hhgaghhgsdhdhdd
      ```
  - Get the coverage with `pytest --cov=src`
  - Run the tests with `py.test --cov=src --cov-config .coveragerc`