import os
import json
import requests
from src.models.UserModel import * #UserModel, UserSchema
from src.app import create_app, db
from flask import request, json, Response, Blueprint, g

#class UsersCreation(self):
"""
Users Test Case
"""

user_api = Blueprint('user_api', __name__)
user_schema = UserSchema()

def setUp(self):
    """
    Test Setup
    """
    self.app = create_app("testing")
    self.client = self.app.test_client
    try:
        response = requests.get("https://randomuser.me/api/")
        #data = response.json()
        data = response.json()
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print(e)
    self.user = [UserModel(name=x['name']['first'], email=x['email']) for x  in data['results']]
    json.dumps(data)
    '''{
    'name': 'kristian',
      'email': 'kristian@mail.com',
      'password': 'passw0rd!'
    }'''


    with self.app.app_context():
      # create all tables
      db.create_all()

@user_api.route('/', methods=['POST'])
def test_user_creation(self):
	""" test user creation with valid credentials """
	res = self.client().post('/api/v1/users/', headers={'Content-Type': 'application/json'}, data=json.dumps(self.user))
	json_data = json.loads(res.data).get('jwt_token')
	#api_token = json.loads(res.data).get('jwt_token')
	#self.assertTrue(json_data.get('jwt_token'))
	#self.assertEqual(res.status_code, 201)

@user_api.route('/', methods=['POST'])
def test_delete_user(self):
	""" Test User Delete """
	res = self.client().post('/api/v1/users/', headers={'Content-Type': 'application/json'}, data=json.dumps(self.user))
	#self.assertEqual(res.status_code, 201)
	api_token = json.loads(res.data).get('jwt_token')
	res = self.client().delete('/api/v1/users/me', headers={'Content-Type': 'application/json', 'api-token': api_token})
	#self.assertEqual(res.status_code, 204)